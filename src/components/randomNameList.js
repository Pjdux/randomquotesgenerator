import React, { useState } from "react";
import namesList from "./namesList";
import "./styles.css";

const AppOriginal = () => {
  const [displayName, setDisplayName] = useState("");

  const randomNumber = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  const random = randomNumber(1, namesList.names.length);
  console.log("khj", namesList.lastNames.length);

  const onFormSubmit = (e) => {
    e.preventDefault();

    const getAllNames = () => {
      const firstNames = namesList.names.map((x, i) => {
        i = i + 1;
        return { index: i, name: x.names };
      });

      const lastNames = namesList.lastNames.map((x, i) => {
        i = i + 1;
        return { index: i, name: x.lastName };
      });

      return;
    };

    const formalName = (firstName, lastName) => {
      const newName =
        Array.from(firstName)[0].toUpperCase() +
        firstName.slice(1) +
        Array.from(lastName)[1].toUpperCase() +
        lastName.slice(1);
      return newName;
    };

    const randomName = () => {
      const returnIndex = getAllNames.map((x) => {
        return (
          x.index === random &&
          /*`ID: ${x.index} Name:*/ ` ${formalName(x.name, x.lastName)}`
        );
      });
      return returnIndex;
    };

    setDisplayName(randomName);
  };

  return (
    <div className="App">
      <form onSubmit={onFormSubmit} className="form-styles">
        <label>Random Name</label>
        <div className="textarea">{displayName}</div>
        <input type="submit" value="Fetch New Name" />
      </form>
    </div>
  );
};

export default AppOriginal;
