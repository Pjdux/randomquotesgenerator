import React, { useEffect, useState } from "react";
import "../App.css";

const QuoteBox = () => {
  const [randomQuote, setRandomQuote] = useState([]);
  const [quoteIndex, setQuoteIndex] = useState(
    Math.floor(Math.random() * 1643)
  );

  useEffect(() => {
    const url = "https://type.fit/api/quotes";
    fetch(url)
      .then((res) => res.json())
      .then((data) => {
        setRandomQuote(data);
      });
  }, []);

  const increaseIndex = () => {
    setQuoteIndex(quoteIndex + 1);
    console.log(randomQuote[quoteIndex]);
  };



  return (
    <div className="quotes-wrap">
      <div id="quote-box">
        <p id="text">
          "{randomQuote[quoteIndex] && randomQuote[quoteIndex].text}"
        </p>
        <span id="author">
          Author{" "}
          <em>
            - {randomQuote[quoteIndex] && randomQuote[quoteIndex].author} -
          </em>
        </span>
        <div className="button-box">
        <button className="new-quote-button" id="new-quote" onClick={increaseIndex}>
          New Quote
        </button>


          <a
            id="tweet-quote"
            href="https://twitter.com/intent/tweet"
            target="_top"
            rel="noopener noreferrer"
          >
<span>Tweet</span>
          </a>

      </div>
      </div>
    
    </div>
  );
};

export default QuoteBox;
