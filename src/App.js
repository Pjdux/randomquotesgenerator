import React, { useState, useEffect } from "react";
import "./App.css";
import QuoteBox from "./components/QuoteBox";

const App = () => {
  return (
    <div className="App-header">
      <h1>Random Quotes</h1>
      <QuoteBox />
    </div>
  );
};

export default App;
