export const quotes = [
  { id: 1, text: "Get rich or die trying...", author: "50 Cent" },
  {
    id: 2,
    text: "Some people feel the rain, others just get wet...",
    author: "Bob Dylan",
  },
  {
    id: 3,
    text: "Life is what happens to you While You're busy making other plans...",
    author: "John Lennon",
  },
  { id: 4, text: "Fly!...", author: "Michael Jordan" },
  { id: 5, text: "I had a dream...", author: "Martin Luther King J.R." },
  { id: 6, text: "Be like water...", author: "Bruce Lee" },
];
